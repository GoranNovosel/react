import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { List, Segment, Grid } from 'semantic-ui-react';


class Hour extends Component{

  constructor(props)
  {
    super(props);
    this.state = { reserved : false, color: 'green' }
    this.handleClick = this.handleClick.bind(this);
  }

  updateParentState = (itemId, action) => {
    this.props.callbackFromParent(itemId, action);
  }

  render(){

    return (
      <List.Item style={{ background: this.state.color }}>
          <List.Content onClick={ () => this.handleClick(this.props.hourId)}>
            {this.props.hourId}:00
            <List.Header >rezerviraj</List.Header>
          </List.Content>
      </List.Item>
    );
  }

  handleClick(itemId)
  {
    if(this.state.reserved === false)
    {
        this.setState(prevState => ({
            reserved: true,
            color: 'orange'
        }));

        this.updateParentState(itemId, 'add')
    }
    else {
      this.setState(prevState => ({
          reserved: false,
          color: 'green'
      }));

      this.updateParentState(itemId, 'remove')
    }
  }
}



export default Hour;
