import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TodoApp from './TodoApp';
import './App.css';


class App extends Component {
  render() {
    return (
        <TodoApp />
    );
  }
}

export default App;
