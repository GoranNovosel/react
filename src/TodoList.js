import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class TodoList extends Component {
  render() {
    return (
      <ul>
        {this.props.items.map(item => (
          <li key={item.id}>{item.text} - {item.id}</li>
        ))}
      </ul>
    );
  }
}

export default TodoList;
