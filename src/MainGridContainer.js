import React from 'react'
import { Grid, Image } from 'semantic-ui-react'
import Day from './Day';

const MainGridContainer = () => (
  <Grid divided='vertically' stackable columns={2}>
    <Grid.Row columns={2} style={{ marginTop: '75px'}}>
      <Grid.Column>
          <Day />
      </Grid.Column>
      <Grid.Column>
        <h1>Placeholder for now</h1>
      </Grid.Column>
    </Grid.Row>
  </Grid>
)

export default MainGridContainer
