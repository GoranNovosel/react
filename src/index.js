import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import MainGridContainer from './MainGridContainer';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<MainGridContainer />, document.getElementById('root'));
registerServiceWorker();
