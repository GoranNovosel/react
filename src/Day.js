import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { List, Segment, Grid } from 'semantic-ui-react';
import Hour from './Hour';
import _ from 'lodash';

class Day extends Component{

  constructor(props)
  {
    super(props);
    let numbers = Array(24).fill().map( (x,i) => i);
    this.state = { hours: numbers, reserved: [] };
  }

  myCallback = (dataFromChild, action) => {

    switch(action)
    {
        case 'add':
          this.setState(prevState => ({
            reserved: _.concat(prevState.reserved, dataFromChild)
          }));
          break;
        case 'remove':
          this.setState(prevState => ({
            reserved: _.remove(prevState.reserved, function(n){
                return n != dataFromChild;
            })
          }));
          break;
    }
    console.log(this.state.reserved);
  }

  render(){
    return (
      <Grid centered>
        <Grid.Row>
          <Grid.Column width={5}>
            <Segment inverted>
              <List divided inverted relaxed>
              { this.state.hours.map(item => (
                 <Hour key={item} hourId={item} callbackFromParent={this.myCallback} />
              ))}
              </List>
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}



export default Day;
